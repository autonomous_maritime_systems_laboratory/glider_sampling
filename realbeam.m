function beam = realbeam

% Assign model space variables
xres=.1;
yres=.1;

% Prompt for echo-sounder details
prompt={'Give the name of the echo-sounder',...
        'Define the echo-sounder sample interval',...
        'Define the echo-sounder beam angle in degrees',...
        'Define the echo-sounder range in metres',...
        'Define the echo-sounder bin interval in metres'...
        };
    name='Define model space';
    numlines=1;
    defaultanswer={'ES853',...
                   '1',...
                   '10',...
                   '100',...
                   '0.5',...
                   };
answer=inputdlg(prompt,name,numlines,defaultanswer);
   % Assign model space variables
unit=answer{1};
deltat=str2double(answer{2});
beamangle=str2double(answer{3});
range=str2double(answer{4});
bininterval=str2double(answer{5});
clear answer

%% Generate the beam 
    % Make the bin ranges
    bins=bininterval:bininterval:range;
    % Calculate bin volumes
    bin_volume=calc_bin_volume('angle',beamangle,'ranges',bins);
    
    % Make a beam grid, using horizonal and vertical extents
    yextent = bins(end) * 1.02; % Add a margin of comfort - will ultimately be blanked
    xextent = 1.1 * yextent * tan(deg2rad(beamangle/2)); % Use the half-beam angle, add yet more margin
    
    % Make dimension vectors
    newvvector = 0:yres:yextent;
    newhvector = -xextent:xres:xextent;
    
    % Make arrays of those vectors
    a = repmat(newvvector',1,length(newhvector));
    b = repmat(newhvector,length(newvvector),1);
    
    % Calculate angle and distance to the origin - polar coordinates
    [cell_angle,cell_range] = cart2pol(b,a);
    
    cell_angle = abs(rad2deg(cell_angle)-90); % Rotate 90 and Convert to absolute degrees
    
    % Shape the beam array
    beamarray = nan(size(cell_angle));
    beamarray(cell_angle<=(beamangle/2)) = 1;
    
    % Trim the ends
    beamarray(cell_range<=(bins(1)-bininterval))=NaN; % Near range
    beamarray(cell_range>(bins(end)+bininterval))=NaN; % Far range
    
    
    
    % Contrain the beam ranges to the beam angle
    beamranges = cell_range.*beamarray; 
    rawbeam = beamranges;
   
    % Bin the ranges to match echo-sounder specs   
    binnumber = nan(size(beamranges));

    for ii = 1:length(bins)
       beamranges(beamranges>bins(ii)-bininterval & beamranges <= bins(ii)+bininterval)=bins(ii);
       binnumber(beamranges>bins(ii)-bininterval & beamranges <= bins(ii)+bininterval)=ii;
    end
    
    % Make a beam shape (binary)
    binarybeam = zeros(size(beamranges));
    binarybeam(beamranges>0) = 1;
    
beam = struct(...
    'bins',bins,...
    'volume',bin_volume,...
    'rawbeam',rawbeam,...
    'binary',binarybeam,...
    'x',newhvector,...
    'y',newvvector,...
    'beamranges',beamranges,...
    'binnumber',binnumber...
    );


%% 