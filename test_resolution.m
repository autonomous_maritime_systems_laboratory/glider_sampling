% This script will test the ability of the glider to resolve targets moving at different velocities
% Damien Guihen, October 2015

%% Declare target
%target=target1;

% Set up the platform spaces
shipprofile.space.depth = 100;
shipprofile.space.distance = 1000;
slocumprofile.space.depth = 100;
slocumprofile.space.distance = 1000;

% Use a common hvelrange - use a variable spacing - concentrating on +- 1
% m/s
common_hvelrange=[-1:.2:-2 , -1.95:0.1:-1 , -.99:.02:.99 ,1:0.1:2, 2.2:.2:3];
%common_hvelrange_extended=[-4:.2:-2 , -1.95:0.1:-1 , -.99:.01:.99 ,1:0.1:2, 2.2:.2:3,3.05:0.05:4];

% Test horizontal velocity at 10 m
disp('------------');
disp('Testing sensivivity to horizontal velocity with 10 m target');
disp(datestr(now));
% Set variables
hvelrange=common_hvelrange;
vvelrange=0;
xdim = [55 65];
ydim = [60 80];
% Test 
disp('   > Testing Ship Platform')
horzvelocitysensitivity.shp_t10m_60to80m=runsensitivity(target,shipprofile,xdim,ydim,hvelrange,vvelrange);
disp('   > Testing Glider Platform')
horzvelocitysensitivity.gld_t10m_60to80m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
disp('   > Testing Glider at 0? Platform')
slocumprofile.platform.diveangle = 0;
horzvelocitysensitivity.gldnodive_t10m_60to80m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
slocumprofile.platform.diveangle = 26;

horzvelocitysensitivity.hvelrange=hvelrange;
horzvelocitysensitivity.ydim=ydim;
%save
save sensitivity_testing_temp

%% Test horizontal velocity with 20 m target
disp('------------');
disp('Testing sensivivity to horizontal velocity with 20 m target');
disp(datestr(now));
% Set variables
hvelrange=common_hvelrange;
vvelrange=0;
xdim = [50 70];
ydim = [60 80];
% Test 
disp('   > Testing Ship Platform')
horzvelocitysensitivity.shp_t20m_60to80m=runsensitivity(target,shipprofile,xdim,ydim,hvelrange,vvelrange);
disp('   > Testing Glider Platform')
horzvelocitysensitivity.gld_t20m_60to80m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
disp('   > Testing Glider at 0? Platform')
slocumprofile.platform.diveangle = 0;
horzvelocitysensitivity.gldnodive_t20m_60to80m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
slocumprofile.platform.diveangle = 26;

horzvelocitysensitivity.hvelrange=hvelrange;
horzvelocitysensitivity.ydim=ydim;
%save
save sensitivity_testing_temp

% %% Test horizontal velocity with 30 m target
% disp('------------');
% disp('Testing sensivivity to horizontal velocity with 30 m target');
% disp(datestr(now));
% % Set variables
% hvelrange=common_hvelrange;
% vvelrange=0;
% xdim = [45 75];
% ydim = [60 80];
% % Test 
% horzvelocitysensitivity.shp_t30m_60to80m=runsensitivity(target,shipprofile,xdim,ydim,hvelrange,vvelrange);
% horzvelocitysensitivity.gld_t30m_60to80m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
% horzvelocitysensitivity.hvelrange=hvelrange;
% horzvelocitysensitivity.ydim=ydim;
% %save
% save sensitivity_testing_temp


%% Test horizontal velocity with 50 m target
disp('------------');
disp('Testing sensivivity to horizontal velocity with 50 m target');
disp(datestr(now));
% Set variables
hvelrange=common_hvelrange;
vvelrange=0;
xdim = [30 80];
ydim = [60 80];
% Test 
disp('   > Testing Ship Platform')
horzvelocitysensitivity.shp_t50m_60to80m=runsensitivity(target,shipprofile,xdim,ydim,hvelrange,vvelrange);
disp('   > Testing Glider Platform')
horzvelocitysensitivity.gld_t50m_60to80m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
disp('   > Testing Glider at 0? Platform')
slocumprofile.platform.diveangle = 0;
horzvelocitysensitivity.gldnodive_t50m_60to80m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
slocumprofile.platform.diveangle = 26;

horzvelocitysensitivity.hvelrange=hvelrange;
horzvelocitysensitivity.ydim=ydim;
%save
save sensitivity_testing_temp

%% Test horizontal velocity with 100 m target
disp('------------');
disp('Testing sensivivity to horizontal velocity with 100 m target');
disp(datestr(now));
% Set variables
hvelrange=common_hvelrange;
vvelrange=0;
xdim = [20 120];
ydim = [60 80];
% Test 
disp('   > Testing Ship Platform')
horzvelocitysensitivity.shp_t100m_60to80m=runsensitivity(target,shipprofile,xdim,ydim,hvelrange,vvelrange);
disp('   > Testing Glider Platform')
horzvelocitysensitivity.gld_t100m_60to80m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
disp('   > Testing Glider at 0? Platform')
slocumprofile.platform.diveangle = 0;
horzvelocitysensitivity.gldnodive_t100m_60to80m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
slocumprofile.platform.diveangle = 26;
horzvelocitysensitivity.hvelrange=hvelrange;
horzvelocitysensitivity.ydim=ydim;
%save
save sensitivity_testing_temp

%% Test horizontal velocity with 150 m target
disp('------------');
disp('Testing sensivivity to horizontal velocity with 150 m target');
disp(datestr(now));
% Set variables
hvelrange=common_hvelrange;
vvelrange=0;
xdim = [10 160];
ydim = [60 80];
% Test 
disp('   > Testing Ship Platform')
horzvelocitysensitivity.shp_t150m_60to80m=runsensitivity(target,shipprofile,xdim,ydim,hvelrange,vvelrange);
disp('   > Testing Glider Platform')
horzvelocitysensitivity.gld_t150m_60to80m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
disp('   > Testing Glider at 0? Platform')
slocumprofile.platform.diveangle = 0;
horzvelocitysensitivity.gldnodive_t150m_60to80m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
slocumprofile.platform.diveangle = 26;


horzvelocitysensitivity.hvelrange=hvelrange;
horzvelocitysensitivity.ydim=ydim;
%save
save sensitivity_testing_temp

%% Test horizontal velocity with 200 m target
disp('------------');
disp('Testing sensivivity to horizontal velocity with 200 m target');
disp(datestr(now));
% Set variables
hvelrange=common_hvelrange;
vvelrange=0;
xdim = [1 201];
ydim = [60 80];
% Test 
disp('   > Testing Ship Platform')
horzvelocitysensitivity.shp_t200m_60to80m=runsensitivity(target,shipprofile,xdim,ydim,hvelrange,vvelrange);
disp('   > Testing Glider Platform')
horzvelocitysensitivity.gld_t200m_60to80m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
disp('   > Testing Glider at 0? Platform')
slocumprofile.platform.diveangle = 0;
horzvelocitysensitivity.gldnodive_t200m_60to80m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
slocumprofile.platform.diveangle = 26;
horzvelocitysensitivity.hvelrange=hvelrange;
horzvelocitysensitivity.ydim=ydim;
%save
save sensitivity_testing_temp

% Test larger velocity for ship
% shipprofile.space.depth=100;
% shipprofile.space.distance=2000;
% disp('------------');
% disp('Testing ship sensivivity to larger horizontal velocities with 50 m target');
% disp(datestr(now));
% Set variables
% hvelrange=0:0.1:8;
% vvelrange=0;
% xdim = [075 525];
% ydim = [60 80];
% Test 
% horzvelocitysensitivity.HIGHshp_t50m_60to80m=runsensitivity(target,shipprofile,xdim,ydim,hvelrange,vvelrange);
% horzvelocitysensitivity.HIGHhvelrange=hvelrange;
% horzvelocitysensitivity.ydim=ydim;
% save
% save sensitivity_testing_temp

% Test vertical velocity range
% disp('------------');
% disp('Testing sensivivity to vertical velocity with 50 m target');
% disp(datestr(now));
% % Set variables
% hvelrange=0;
% vvelrange=-.2:0.01:.2;
% xdim = [30 80];
% ydim = [80 100];
% % Test 
% vertvelocitysensitivity.shp_t50m_80to100m=runsensitivity(target,shipprofile,xdim,ydim,hvelrange,vvelrange);
% vertvelocitysensitivity.gld_t50m_80to100m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
% vertvelocitysensitivity.hvelrange=hvelrange;
% vertvelocitysensitivity.ydim=ydim;
% %save
% save sensitivity_testing_temp

% %% Test target width
% shipprofile.space.depth=300;
% shipprofile.space.distance=300;
% disp('------------');
% disp('Testing sensivivity to target width');
% disp(datestr(now));
% % Set variables
% hvelrange=0;
% vvelrange=0;
% xdim = [55,65;50,70;45,75;30,80;20,120;10,160;1,201];
% ydim = [80 100];
% % Test 
% lengthsensitivity.shp_txm_80to100m=runsensitivity(target,shipprofile,xdim,ydim,hvelrange,vvelrange);
% lengthsensitivity.gld_txm_80to100m=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
% lengthsensitivity.hvelrange=hvelrange;
% lengthsensitivity.ydim=ydim;
% lengthsensitivity.xdim=xdim;
% % Save
% save sensitivity_testing_temp
% 
% 
% %% Test target depth
% disp('------------');
% disp('Testing sensivivity to target depth');
% disp(datestr(now));
% % Set variables
% hvelrange=0;
% vvelrange=0;
% xdim = [0,20];
% a=(10:10:180);b=a+20;ydim=[a',b'];
% % Test 
% depthsensitivity.shp_t20m_xtoxm=runsensitivity(target,shipprofile,xdim,ydim,hvelrange,vvelrange);
% depthsensitivity.gld_t20m_xtoxm=runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
% depthsensitivity.hvelrange=hvelrange;
% depthsensitivity.ydim=ydim;
% % Save
% save sensitivity_testing_temp

%% Clean up
clear hvelrange vvelrange xdim a b ydim target ans
%% Finished
disp('------------');
disp('Finished');
disp(datestr(now));