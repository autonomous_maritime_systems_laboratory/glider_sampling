# README
This is an acoustic sampling model that was developed to simulate the appearance of targets within a dataset collected by a glider with a downward-facing echo-sounder. The imagined echo-sounder has a single-beam, single-frequency but the angle, range, bin interval, sampling interval, beam pattern sensitivity and angular dependance can be configured. The platform speed (left to right) and diving angle (if any) can also be configured. The sampling space can be configured as required with variable horizontal and vertical extents and independent horizontal and vertical resolution if required.

The operation and function of this repository is described in 
[Guihen D \(2018\) High-resolution acoustic surveys with diving gliders come at a cost of aliasing moving targets. PLoS ONE 13\(8\): e0201816.](https://doi.org/10.1371/journal.pone.0201816)
## What is this repository for? 
This repository is to test ideas of sampling efficacy for different platforms, echo-sounders and targets. There are already some very detailed studies of the performance of different echo-sounders and their use in pelagic ecology. The idea behind this repository is to provide code to look at how these echo-sounders might be used on platforms like buoyancy-driven gliders. Appropriate echo-sounders are still quite simple but as they develop, or with the need to design new instruments, the necessary sampling behaviour can be coded into this simulation. 

The primary code is written as a single Matlab function, thus no dependancies are required. Future refinements might mean that it is necessary to break this growing function into smaller functions to make it more-easily extensible. The *realbeam.m* function is an example of how  this might be broken up in the future, with individual functions handling platform, beam, target evolution etc. 

The first commit of the code to Git is Version 2 of the project. Version 1 focussed on spatial coverage and simple target sampling. Version 2 brought the movement of targets relative to the platform and a more realistic beam, along with the ability to define differential sensitivity within this beam.

## Contents
- README.m: This document
- acoustic_sampling_model.m: the function for simulating sampling 
- targets.mat: sample targets for use with the function
- platform_profiles.mat: sample platforms for use with the function
- test_resolution.m: example script for iterating sampling under different conditions
- clearangularbias.m: script clear the angular dependence of the ship and glider profiles (as provided in platform_profiles.mat)
- depth_width_target_test.m: example script for iterating sampling of a target of different widths, at different depths.
- realbeam.m: a function to generate a beam with bias (as in acoustic_sampling_model.m). This is an example of a function that might be useful if the main function is split into several modularised functions.

## How do I get set up?
### Basic operation
The most simple implementation can be tested by running *acoustic_sampling_model* within Matlab. This function presents a GUI that can be used to enter the parameters for the simulation. The GUI presents the following screens:
- Define model space
- Define platform
- Define echo-sounder

Default answers are provided in the GUI that will return answers from a small grid quickly, so clicking okay on each screen should generate a structure with the following sub-structures (explained below):
- grid
- space
- platform
- echosounder
- beam
- flight
- target
- measurement
- analysis

### The command line
Up to 4 (0,1,2 or 4) arguments can be passed to the command line in the following order:
`f = acoustic_sampling_model(target,profile,xdim,ydim);`
- *target* specifies the variable with target information
- *profile* specifies the variable with platform information
- *xdim* & *ydim* can be used to override the size information in the target variable and is useful for iterative simulations.

If a target variable is passed to the function but no platform information, a GUI will be presented to ask questions of the sampling. This GUI is similar to the one discussed earlier but includes some additional dialogues. The following dialogues are presented:
- Define model space
- Define platform
- Define echo-sounder
- Apply target strength angular dependence [y / n]
	- (If Yes) Angular dependence polynomial - up to 10 factors
- Choose beam pattern [Perfect Cone / 3 dB]
- Signal loss at max range (0-1)
### Beam Bias
Beam bias can be applied in the options described in the GUI, or passed in as platform variables. The beam shape is formed from the echo-sounder characteristics passed to the function and is used as a mask for sub-sampling the simulation space. Biases are then added to this beam to reflect different angles of incidence across the beam (particularly pertinent for wide beam angles), the idealised beam definition and range effects. Biases are applied in linear, normalised space: beam maps are made with values of 0-1 indicating the relative sensitivities of the mask.

#### Apply Target Strength Angular Dependence
A target strength model should be used to apply the angular dependence. Use a target strength model to extract the backscattering strength of the target at different angles, to include the angles covered by the simulated beam, and fit a polynomial in linear space. Up to 10 factors can be included and added either in the GUI or the platform definition structure. The angular dependency polynomial factors are cumbersome to clear in the command line so the script *clearangularbias.m* can be used to reset these biases to 0.

#### Choose beam pattern
The beam pattern used can be a simple, perfect echo-sounder (equally sensitive across its horizontal range) or an idealised beam. The 3 dB beam is most sensitive in the middle (-0 dB), and approximately half as sensitive (equating to -3 dB) at the edges.

#### Signal loss at max range
**This is a work in progress and at the moment is experimental.** It would be useful to simulate the loss of signal from an echo-sounder with range. This feature attempts to simulate realistic signal loss in the following manner: 
`rbias=1-(bin_volume/max(bin_volume))*rangeinsensitivity;`
This is a first approximation of signal loss and will be revised in future writing.

### Targets
Targets can be positioned within the simulation by specifying them in the command line (replacing *target* with the variable name of the target to be tested).
`f = acoustic_sampling_model(target);`
The target is defined in a structure with the following variables:
- target: a n*m grid of values between 0 and 1
- xdim: a 1*2 vector containing horizontal extents in meters
- ydim: a 1*2 vector containing vertical extents in meters
- targetvelx: a horizontal velocity in m/s
- targetvely: a vertical velocity in m/s

The *xdim* and *ydim* are used both the set the target size and the position within the simulation. The target will be appropriately resampled within the function for both sizing and resolution of the grid.

### Output
#### *grid*
Contains a measurement of number of times each cell in the simulation has been sampled (*pingcount*), accounting for beam biases (*pingbias*), along with horizontal and vertical vectors (*xax* & *yax*).

#### *space*
Contains the information on the space generated: depth, distance, vertical and horizontal resolutions and the time interval.

#### *platform*
Contains the information provided on the platform: name, dive angle, horizontal velocity (*hvelocity*) and any horizontal or vertical offsets of the echo-sounder.

#### *echosounder*
Contains the information provided on the echo-sounder: unit name, sample interval, beam angle, range and bin interval.

#### *beam*
Contains the information provided and calculated for the echo-sounder beam: bin ranges, blanking distance, bin volumes, beam biases, horizontal and vertical vectors and bin numbers.

#### *flight*
A simple, calculated path for the vehicle given platform variables and timing interval: time, x and y positions with associated intermediate stages (this is mostly used for debugging).

#### *target*
Contains the target information as provided in the input structure, along with intermediate stages that are used for debugging.

#### *measurement*
Contains the 'collected' data. *Collection* maps where the beam has touched in the space. *echogram* is the 2 dimensional data as the vehicle would store it (range bins * pings). *track* & *range* define the horizontal and vertical spatial scales. *pingsignal* is each individual bin value in series, with *pingx* and *pingy* denoting along_track and depth information.

#### *analysis*
This is present only if there is a target in the simulation and contains a first analysis of the data as 'collected', in comparison with seeded information.
- *apparent_length_percent*: apparent flight as a percentage of vehicle travel (clarify?)
- *coveragebins*: percentage of the space sampled in 5 m depth intervals 
- *percentagebins*: percentage of samples on target by bin
- *percentmeasured*: summed observed as percentage of sum seeded
- *profiletarget*: mean vertical profile of target
- *profilemeasurement*: mean vertical profile of observation
- *profiledepth*: depth vector for profiles
- *numtargets*: Number of seeded targets (calculated with bwboundaries)
- *numobservedtargets*: Number of observed targets (calculated with bwboundaries)
- *griddedtarget*: Target intermediate step (for debugging)
- *griddedmeasurement*: Space intermediate step (for debugging)
- *xgrid*: Horizontal vector for grid
- *ygrid*: Vertical vector for grid
- *targetdifference*: Difference between target and observed space

## Who do I talk to?
* Contact Damien Guihen, Australian Maritime College, Univeristy of Tasmania (damien.guihen(at)utas.edu.au)
* Repo created under the Antarctic Gateway Partnership SRI of the Australian Research Council