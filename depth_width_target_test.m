target=target2;

%% Test target depth 
% Set up general parameters
hvelrange=0;
vvelrange=0;
a=(150:10:520);b=a+10;ydim=[a',b'];
depthsensitivity.hvelrange=hvelrange;
depthsensitivity.ydim=ydim;

% Show messages
disp('------------');
disp('Testing sensivivity to target depth');


%Set up the target with vector
twidtharray=5:5:150;

depthsensitivity.depths=a;
depthsensitivity.twidtharray=twidtharray;
%depthsensitivity.jcrtestarray = nan(length(a),length(twidtharray));
%depthsensitivity.gldtestarray = nan(length(a),length(twidtharray));
depthsensitivity.jcrtestarray_deep = nan(length(a),length(twidtharray));
disp(['There are ',num2str(length(twidtharray)*length(a)),' tests to run on each platform']);
disp('Thank goodness for parallel processing...');
h=waitbar(0,'Running simulation - please wait');
% Check widths
for ii=1:length(twidtharray)
    twidth=twidtharray(ii);
    disp('------------');
    disp(['Testing target width: ',num2str(twidth)]);
    disp(datestr(now));
    
    % Set variables
    xdim = [0,twidth];

    % Test 
    jcr = runsensitivity(target,jcrprofile,xdim,ydim,hvelrange,vvelrange);
   % gld = runsensitivity(target,slocumprofile,xdim,ydim,hvelrange,vvelrange);
    
    eval(['depthsensitivity.jcr_t',num2str(twidth),'m_xtoxm=jcr;']);
   % eval(['depthsensitivity.gld_t',num2str(twidth),'m_xtoxm=gld;']);

    
    depthsensitivity.jcrtestarray_deep(:,ii)=jcr(:,1);
    %depthsensitivity.gldtestarray(:,ii)=gld(:,1);
    
waitbar(ii/length(twidtharray),h);
end
close (h)
clear a b ans hvelrange vvelrange twidtharray twidth xdim ydim
save ../depth-width-sensitivity_deep
